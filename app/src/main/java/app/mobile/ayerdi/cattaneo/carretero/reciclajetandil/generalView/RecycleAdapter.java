package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.generalView;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.R;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.addRecycle.RecycleModel;

public class RecycleAdapter extends ArrayAdapter<RecycleModel> implements View.OnClickListener {

    private ArrayList<RecycleModel> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtFecha;
        TextView txtNro1;
        TextView txtNro2;
        TextView txtNro3;
        TextView txtNro4;
        TextView txtNro5;
    }

    public RecycleAdapter(ArrayList<RecycleModel> data, Context context) {
        super(context, R.layout.row_item , data);
        this.dataSet = data;
        this.mContext=context;
    }


    @Override
    public void onClick(View v) {
        int position=(Integer) v.getTag();
        Object object= getItem(position);
        RecycleModel dataModel=(RecycleModel)object;
    }

    private int lastPosition = -1;

    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        RecycleModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {
            Log.d("TEST_COVERT", "Convert View == null");
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item, parent, false);
            viewHolder.txtFecha = (TextView) convertView.findViewById(R.id.txtFechaRowItem);
            viewHolder.txtNro1 = (TextView) convertView.findViewById(R.id.nro1);
            viewHolder.txtNro2 = (TextView) convertView.findViewById(R.id.nro2);
            viewHolder.txtNro3 = (TextView) convertView.findViewById(R.id.nro3);
            viewHolder.txtNro4 = (TextView) convertView.findViewById(R.id.nro4);
            viewHolder.txtNro5 = (TextView) convertView.findViewById(R.id.nro5);


            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }
        // Return the completed view to render on screen
        viewHolder.txtFecha.setText(dataModel.getFecha());
        viewHolder.txtNro1.setText(String.valueOf(dataModel.getCantidadBotella()));
        viewHolder.txtNro2.setText(String.valueOf(dataModel.getCantidadVidrio()));
        viewHolder.txtNro3.setText(String.valueOf(dataModel.getCantidadPapel()));
        viewHolder.txtNro4.setText(String.valueOf(dataModel.getCantidadLata()));
        viewHolder.txtNro5.setText(String.valueOf(dataModel.getCantidadTetra()));
        return convertView;

    }


    }

package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.login;

import android.content.Context;
import android.content.SharedPreferences;

public class UtilsSharedPreference {

    public static final String TP_RECICLADOTANDIL_SHARED_PREFERENCE = "TP_RECICLADOTANDIL_SHARED_PREFERENCE";

    public static void save (Context ctx, String name, String value){
        SharedPreferences s = ctx.getSharedPreferences(TP_RECICLADOTANDIL_SHARED_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= s.edit();
        editor.putString(name, value);
        editor.apply();
    }

    public static String read (Context ctx, String name, String value){
        SharedPreferences s = ctx.getSharedPreferences(TP_RECICLADOTANDIL_SHARED_PREFERENCE, Context.MODE_PRIVATE);
        return s.getString(name, value);
    }

    public static void clear(Context ctx){
        SharedPreferences s = ctx.getSharedPreferences(TP_RECICLADOTANDIL_SHARED_PREFERENCE, Context.MODE_PRIVATE);
        s.edit().clear().commit();
    }

}

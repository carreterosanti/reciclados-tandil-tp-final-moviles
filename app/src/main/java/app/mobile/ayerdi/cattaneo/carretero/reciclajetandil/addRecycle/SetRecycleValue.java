package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.addRecycle;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.R;

public class SetRecycleValue extends Activity {

    public final static String SET_VALUE = "SET_VALUE";
    private TextView txtValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_recycle_value);
        txtValue = (TextView) findViewById(R.id.txtValue);
        final Intent intent = getIntent();
        txtValue.setText(intent.getStringExtra(SET_VALUE));
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        txtValue.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                switch (actionId) {

                    case EditorInfo.IME_ACTION_DONE:
                        Intent data = new Intent();
                        if (!txtValue.getText().toString().matches("")){
                            data.setData(Uri.parse(txtValue.getText().toString()));
                            setResult(RESULT_OK, data);
                        } else {
                            setResult(RESULT_CANCELED, data);
                        }
                        finish();
                        return true;

                    default:
                        return false;
                }
            }
        });
    }


}

package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.addRecycle;

public class TotalRecycleModel {
    private double tons;
    private int bottles;
    private int tetrabriks;
    private int glass;
    private int paperboard;
    private int cans;

    public TotalRecycleModel(double tons, int bottles, int tetrabriks, int glass, int paperboard, int cans) {
        this.tons = tons;
        this.bottles = bottles;
        this.tetrabriks = tetrabriks;
        this.glass = glass;
        this.paperboard = paperboard;
        this.cans = cans;
    }

    public double getTons() {
        return tons;
    }

    public int getBottles() {
        return bottles;
    }

    public int getTetrabriks() {
        return tetrabriks;
    }

    public int getGlass() {
        return glass;
    }

    public int getPaperboard() {
        return paperboard;
    }

    public int getCans() {
        return cans;
    }
}

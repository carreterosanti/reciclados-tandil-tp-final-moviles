package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.login.newUser;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.util.Patterns;

import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.generalView.GeneralViewRecycle;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.R;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.UserModel;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.system.ColoredSnackbar;

public class NewUserActivity extends AppCompatActivity {

    ImageView iconNewUser;
    TextView txtName;
    TextView txtLastName;
    TextView txtEmail;
    TextView txtUsername;
    Button btnQuickLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);
        getSupportActionBar().hide(); //hide the title bar

        txtName = (TextView) findViewById(R.id.txtNombre);
        txtLastName = (TextView) findViewById(R.id.txtApellido);
        txtEmail = (TextView) findViewById(R.id.txtEmailManager);
        txtUsername = (TextView) findViewById(R.id.txtUsername);
        btnQuickLogin = (Button) findViewById(R.id.btnQuickLogin);
        iconNewUser = (ImageView) findViewById(R.id.iconNewUser);

        final Button btnSiguiente = (Button) findViewById(R.id.btnSiguienteLogIn);
        final Intent newUserMapAddress = new Intent(this, NewUserMapAddressActivity.class);

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence email = txtEmail.getText().toString();
                if (!txtName.getText().toString().matches("") && !txtLastName.getText().toString().matches("") && !txtEmail.getText().toString().matches("") && !txtUsername.getText().toString().matches("") && (Patterns.EMAIL_ADDRESS.matcher(email).matches())) {
                        newUserMapAddress.putExtra(GeneralViewRecycle.USER_INITIALS, btnQuickLogin.getText());
                        newUserMapAddress.putExtra(GeneralViewRecycle.USER_MODEL, new UserModel(txtName.getText().toString(), txtLastName.getText().toString(), txtEmail.getText().toString(), txtUsername.getText().toString(), null,null,null,null,null,null));
                        startActivity(newUserMapAddress); //Siguiente activity NewUserMapAddress
                } else if (txtName.getText().toString().matches("")){ //Snackbar correspondiente al campo incompleto!
                    ColoredSnackbar.showColoredSnackBar(v, getString(R.string.errorSnackName),R.color.colorErrorSnackbar, Snackbar.LENGTH_SHORT);
                } else if (txtLastName.getText().toString().matches("")){
                    ColoredSnackbar.showColoredSnackBar(v, getString(R.string.errorSnackLastName),R.color.colorErrorSnackbar, Snackbar.LENGTH_SHORT);
                } else if (txtEmail.getText().toString().matches("")){
                    ColoredSnackbar.showColoredSnackBar(v, getString(R.string.errorSnackEmail),R.color.colorErrorSnackbar, Snackbar.LENGTH_SHORT);
                } else if (txtUsername.getText().toString().matches("")){
                    ColoredSnackbar.showColoredSnackBar(v, getString(R.string.errorSnackUsername),R.color.colorErrorSnackbar, Snackbar.LENGTH_SHORT);
                } else {
                    ColoredSnackbar.showColoredSnackBar(v, getString(R.string.errorSnackValidarEmail),R.color.colorErrorSnackbar, Snackbar.LENGTH_SHORT);

                }

            }
        });

        setLetrasQuickLogin();
    }

    private void setLetrasQuickLogin(){
        // PERMITE VERIFICAR EL CAMBIO DEL ONFOCUS SOBRE EL EDITTEXT PARA SETEAR EL QUICKLOGIN
        ((EditText)findViewById(R.id.txtNombre)).setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                /* When focus is lost check that the text field
                 * has valid values.
                 */
                if (!hasFocus) {
                    Button btnQuickLogin = (Button) findViewById(R.id.btnQuickLogin);
                    if (!txtName.getText().toString().matches("")){ // Si el nombre no esta vacio
                        if (txtLastName.getText().toString().matches("")){ // Pregunta si esta completo el apellido
                            Log.d("TEST", "Sin apeelido");
                            hideIconNewUser();
                            btnQuickLogin.setText(String.valueOf(txtName.getText().charAt(0))); // Si todavia no esta completo: Cambio el QuickLogin solo con la inicial del nombre
                        } else
                        {
                            Log.d("TEST", "Con apeelido");
                            hideIconNewUser();
                            btnQuickLogin.setText(String.valueOf(txtName.getText().charAt(0) + String.valueOf(txtLastName.getText().charAt(0)))); //Si el apellido esta completo: Cambio el QuickLogin con ambas iniciales
                        }
                    } else {
                        if (txtLastName.getText().toString().matches("")){ // Pregunta si esta completo el apellido
                            btnQuickLogin.setText(""); // Si todavia no esta completo: Cambio el QuickLogin solo con la inicial del nombre
                            showIconNewUser();
                        } else
                        {
                            hideIconNewUser();
                            btnQuickLogin.setText(String.valueOf("" + String.valueOf(txtLastName.getText().charAt(0)))); //Si el apellido esta completo: Cambio el QuickLogin con ambas iniciales
                        }
                    }
                }
            }
        });

        // PERMITE VERIFICAR EL CAMBIO DEL ONFOCUS SOBRE EL EDITTEXT PARA SETEAR EL QUICKLOGIN
        ((EditText)findViewById(R.id.txtApellido)).setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                /* When focus is lost check that the text field
                 * has valid values.
                 */
                if (!hasFocus) {
                    Button btnQuickLogin = (Button) findViewById(R.id.btnQuickLogin);
                    if (!txtLastName.getText().toString().matches("")){ // Si el apellido esta no vacio
                        if (txtName.getText().toString().matches("")){ // Pregunta si esta completo el nombre
                            hideIconNewUser();
                            btnQuickLogin.setText(String.valueOf(txtLastName.getText().charAt(0))); // Si todavia no esta completo: Cambio el QuickLogin solo con la inicial del apellido
                        } else
                        {
                            hideIconNewUser();
                            btnQuickLogin.setText(String.valueOf(txtName.getText().charAt(0) + String.valueOf(txtLastName.getText().charAt(0)))); //Si el apellido esta completo: Cambio el QuickLogin con ambas iniciales
                        }
                    } else {
                        if (txtName.getText().toString().matches("")){ // Pregunta si esta completo el nombre
                            btnQuickLogin.setText(""); // Si todavia no esta completo: Cambio el QuickLogin solo con la inicial del apellido
                            showIconNewUser();
                        } else
                        {
                            hideIconNewUser();
                            btnQuickLogin.setText(String.valueOf(txtName.getText().charAt(0) + "")); //Si el apellido esta completo: Cambio el QuickLogin con ambas iniciales
                        }
                    }
                }
            }
        });
    }


    private void hideIconNewUser(){
        iconNewUser.setVisibility(View.INVISIBLE);
    }

    private void showIconNewUser(){
        iconNewUser.setVisibility(View.VISIBLE);
    }
}

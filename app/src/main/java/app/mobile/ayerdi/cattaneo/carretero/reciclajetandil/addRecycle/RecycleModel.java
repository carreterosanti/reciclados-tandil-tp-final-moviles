package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.addRecycle;

public class RecycleModel {

    private String fecha;
    private int cantidadBotella;
    private int cantidadPapel;
    private int cantidadVidrio;
    private int cantidadLata;
    private int cantidadTetra;

    public RecycleModel(String fecha, int cantidadBotella, int cantidadPapel, int cantidadVidrio, int cantidadLata, int cantidadTetra){
        this.fecha = fecha;
        this.cantidadBotella = cantidadBotella;
        this.cantidadPapel = cantidadPapel;
        this.cantidadVidrio = cantidadVidrio;
        this.cantidadLata = cantidadLata;
        this.cantidadTetra = cantidadTetra;
    }


    public String getFecha() {
        return fecha;
    }

    public int getCantidadBotella() {
        return cantidadBotella;
    }

    public int getCantidadPapel() {
        return cantidadPapel;
    }

    public int getCantidadVidrio() {
        return cantidadVidrio;
    }

    public int getCantidadLata() {
        return cantidadLata;
    }

    public int getCantidadTetra() {
        return cantidadTetra;
    }


}

package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.generalView;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.UserModel;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.system.ColoredSnackbar;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.login.MainActivity;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.login.UtilsSharedPreference;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.login.newUser.NewUserMapAddressActivity;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.R;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.UserManagerActivity;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.addRecycle.AddRecycle;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.addRecycle.RecycleModel;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.addRecycle.TotalRecycleModel;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.services.RecycleResponse;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.services.RecycleService;

public class   GeneralViewRecycle extends AppCompatActivity {

    public final static String USER_INITIALS = "USER_INITIALS";
    public final static String USER_MODEL = "USER_MODEL";
    public static final String USERNAME = "USERNAME";
    public final static String BOTELLA = "BOTELLA";
    public final static String PAPEL = "PAPEL";
    public final static String FECHA = "FECHA";
    public final static String TETRABRICK = "TETRABRICK";
    public final static String LATA = "LATA";
    public final static String VIDRIO = "VIDRIO";

    // GRAFICO DE TORTA
    private PieChart pieChart;
    private float[] yData = {0.0f,0.0f,0.0f,0.0f,0.0f};

    private ImageView btnSettings;
    private String username;

    private RecycleResponse callback;
    private RecycleResponse callbackTotal;

    ArrayList<RecycleModel> dataModels;
    ListView listView;
    private TextView textFirstRecycle;
    private static RecycleAdapter adapter;

    private Intent recycleItemView;

    boolean doubleBackToExitPressedOnce = false;
    private static boolean firstTimeUser = true;

    public static void setfirstTimeUser(boolean b){
        firstTimeUser = b;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_view_recycle);
        getSupportActionBar().hide(); //hide the title bar
        final Intent intent = getIntent();

        username = UtilsSharedPreference.read(getApplicationContext(), "username", null);


        // USER MANAGER ACTIVITY
        final Intent userManager = new Intent(this, UserManagerActivity.class);
        btnSettings = (ImageView) findViewById(R.id.btnSettings);
        btnSettings.bringToFront();
        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(userManager);
            }
        });

        // MENSAJE DE BIENVENIDA AL USUARIO
        if (intent.getBooleanExtra(NewUserMapAddressActivity.NEW_USER, false) && firstTimeUser){
            ColoredSnackbar.showColoredSnackBar(findViewById(R.id.btnSettings),getString(R.string.completeSnackLogin), R.color.colorNewUserSnackbar, Snackbar.LENGTH_LONG);
            firstTimeUser = false;
        }

        // EMOJIS UNICODE PARA DETALLE DEL GRAFICO
        int unicodeBotella = 0x1F37E;
        int unicodePapel = 0x1F4F0;
        int unicodeVidrio = 0x1F377;
        int unicodeLata = 0x1F96B;
        int unicodeTetra = 0x1F4E6;
        TextView emjTop1 = (TextView) findViewById(R.id.emjTop1);
        TextView emjTop2 = (TextView) findViewById(R.id.emjTop2);
        TextView emjTop3 = (TextView) findViewById(R.id.emjTop3);
        TextView emjTop4 = (TextView) findViewById(R.id.emjTop4);
        TextView emjTop5 = (TextView) findViewById(R.id.emjTop5);
        emjTop1.setText(getEmojiByUnicode(unicodeBotella));
        emjTop2.setText(getEmojiByUnicode(unicodePapel));
        emjTop3.setText(getEmojiByUnicode(unicodeVidrio));
        emjTop4.setText(getEmojiByUnicode(unicodeLata));
        emjTop5.setText(getEmojiByUnicode(unicodeTetra));


        // ADDRECYCLE ACTIVITY
        final Intent addRecycle = new Intent(this, AddRecycle.class);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.btnAddRecycle);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(addRecycle);
            }
        });

        // INICIALIZACION TEXTO PRIMER RECICLADO Y LISTA DE RECICLADOS
        textFirstRecycle = (TextView) findViewById(R.id.textFirstRecycle);
        listView=(ListView)findViewById(R.id.listRecycleItems);

        // INICIALIZACION GRAFICO
        pieChart = (PieChart) findViewById(R.id.pieChart);
        pieChart.setNoDataText(getString(R.string.txtPieChartNoText));

    }

    @Override
    protected void onResume() {
        super.onResume();
        setListViewRecicladosRegistrados();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
        }

        this.doubleBackToExitPressedOnce = true;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    private String getEmojiByUnicode(int unicode){
        return new String(Character.toChars(unicode));
    }


    // ACTUALIZACION COMPLETA DE LA INFORMACION DE RECICLADOS
    private void setListViewRecicladosRegistrados(){
        recycleItemView = new Intent(this, RecycleItemView.class);
        initCallback();
        RecycleService recycleService = new RecycleService(this, callback);
        recycleService.getAllRecyclingUser(username);
        initCallbackTotal();
        RecycleService recycleService1 = new RecycleService(this, callbackTotal);
        recycleService1.getTotalRecyclingUser(username);
    }

    private void addDataSet() {
        ArrayList<String> xEntrys = new ArrayList<>();
        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        for(int i = 0; i < yData.length; i++){
            yEntrys.add(new PieEntry(yData[i] , i));
        }

        //create the data set
        PieDataSet pieDataSet = new PieDataSet(yEntrys, "Reciclados");
        pieDataSet.setSliceSpace(0);
        pieDataSet.setValueTextSize(12);
        pieDataSet.setValueTextColor(getResources().getColor(R.color.colorWhite));
        //add colors to dataset
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(getResources().getColor(R.color.colorPieBotella));
        colors.add(getResources().getColor(R.color.colorPiePapel));
        colors.add(getResources().getColor(R.color.colorPieVidrio));
        colors.add(getResources().getColor(R.color.colorPieLata));
        colors.add(getResources().getColor(R.color.colorPieTetra));
        pieDataSet.setColors(colors);
        pieChart.getLegend().setEnabled(false);
        pieChart.getDescription().setEnabled(false);
        //create pie data object
        PieData pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieChart.invalidate();
        pieChart.setElevation(10);
    }


    // CONSULTA PARA ACTUALIZAR LA LISTA DE RECICLADOS
    void initCallback(){
        this.callback = new RecycleResponse() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                dataModels = (ArrayList<RecycleModel>) response;

                adapter= new RecycleAdapter(dataModels,getApplicationContext());
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        RecycleModel dataModel= dataModels.get(position);
                        recycleItemView.putExtra(BOTELLA,dataModel.getCantidadBotella());
                        recycleItemView.putExtra(PAPEL, dataModel.getCantidadPapel());
                        recycleItemView.putExtra(FECHA, dataModel.getFecha());
                        recycleItemView.putExtra(TETRABRICK, dataModel.getCantidadTetra());
                        recycleItemView.putExtra(LATA, dataModel.getCantidadLata());
                        recycleItemView.putExtra(VIDRIO, dataModel.getCantidadVidrio());
                        startActivity(recycleItemView);
                    }
                });


                ArrayList<RecycleModel> reciclados = (ArrayList<RecycleModel>) response;
                for (RecycleModel r: reciclados){
                    Log.d("CALLBACK", r.getFecha());
                    Log.d("CALLBACK", String.valueOf(r.getCantidadBotella()));
                    Log.d("CALLBACK", String.valueOf(r.getCantidadLata()));
                    Log.d("CALLBACK", String.valueOf(r.getCantidadPapel()));
                    Log.d("CALLBACK", String.valueOf(r.getCantidadTetra()));
                    Log.d("CALLBACK", String.valueOf(r.getCantidadVidrio()));
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d("CALLBACK", error.toString());
            }
        };
    }

    // CONSULTA PARA ACTUALIZAR EL GRAFICO
    void initCallbackTotal(){
        this.callbackTotal = new RecycleResponse() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                TotalRecycleModel total = (TotalRecycleModel) response;
                if (total.getTons() != 0){
                    textFirstRecycle.setVisibility(View.INVISIBLE);
                    yData[0] = Float.valueOf((float) ((total.getBottles() * 100) / (total.getTons())));
                    yData[1] = Float.valueOf((float) ((total.getCans() * 100) / (total.getTons())));
                    yData[2] = Float.valueOf((float) ((total.getGlass() * 100) / (total.getTons())));
                    yData[3] = Float.valueOf((float) ((total.getPaperboard() * 100) / (total.getTons())));
                    yData[4] = Float.valueOf((float) ((total.getTetrabriks() * 100) / (total.getTons())));
                    pieChart.setHoleRadius(0);
                    pieChart.setTransparentCircleRadius(0);
                    addDataSet();
                }
                Log.d("CALLBACK", response.toString());
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d("CALLBACK", error.toString());
            }
        };
    }
}

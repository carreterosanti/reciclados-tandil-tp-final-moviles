package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.login.newUser;


import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.VolleyError;

import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.generalView.GeneralViewRecycle;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.R;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.UserModel;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.services.RecycleResponse;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.services.RecycleService;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.system.ColoredSnackbar;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.login.UtilsSharedPreference;


public class NewUserMapAddressActivity extends AppCompatActivity {

    public final static String NEW_USER = "NEW_USER";

    private EditText txtProvincia;
    private EditText txtCiudad;
    private EditText txtCodigoPostal;
    private EditText txtDireccion;
    private Button btnSiguienteMapLogin;
    private UserModel userModel;

    private RecycleResponse callbackRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user_map_address);
        getSupportActionBar().hide(); //hide the title bar
        final Intent intent = getIntent();
        final Intent generalViewRecycle = new Intent(this, GeneralViewRecycle.class);
        txtProvincia = (EditText) findViewById(R.id.txtProvincia);
        txtCiudad = (EditText) findViewById(R.id.txtCiudad);
        txtCodigoPostal = (EditText) findViewById(R.id.txtCodigoPostal);
        txtDireccion = (EditText) findViewById(R.id.txtDireccion);
        btnSiguienteMapLogin = (Button) findViewById(R.id.btnSiguienteMapLogin);

        userModel = (UserModel) intent.getSerializableExtra(GeneralViewRecycle.USER_MODEL);

        btnSiguienteMapLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!txtProvincia.getText().toString().matches("") && !txtCiudad.getText().toString().matches("") && !txtCodigoPostal.getText().toString().matches("") && !txtDireccion.getText().toString().matches("") ) {
                    generalViewRecycle.putExtra(NEW_USER, true);
                    userModel.setState(txtProvincia.getText().toString());
                    userModel.setCity(txtCiudad.getText().toString());
                    userModel.setZipCode(txtCodigoPostal.getText().toString());
                    userModel.setStreetAddress(txtDireccion.getText().toString());
                    //request al server para guardar el nuevo user
                    initCallbackRegister();
                    RecycleService recycleService = new RecycleService(getApplicationContext(), callbackRegister);
                    recycleService.registerUser(userModel);
                    saveNewUser(userModel); // Se almacena la informacion del usuario mediante SharedPreference
                    startActivity(generalViewRecycle); //Siguiente activity NewUserMapAddress

                }
                if (txtProvincia.getText().toString().matches("")){ //Snackbar correspondiente al campo incompleto!
                    ColoredSnackbar.showColoredSnackBar(v, getString(R.string.errorSnackProvincia),R.color.colorErrorSnackbar,Snackbar.LENGTH_SHORT);
                } else if (txtCiudad.getText().toString().matches("")){
                    ColoredSnackbar.showColoredSnackBar(v, getString(R.string.errorSnackCiudad),R.color.colorErrorSnackbar,Snackbar.LENGTH_SHORT);
                    } else if (txtCodigoPostal.getText().toString().matches("")){
                    ColoredSnackbar.showColoredSnackBar(v, getString(R.string.errorSnackCodigoPostal),R.color.colorErrorSnackbar,Snackbar.LENGTH_SHORT);
                    } else if (txtDireccion.getText().toString().matches("")){
                    ColoredSnackbar.showColoredSnackBar(v, getString(R.string.errorSnackDireccion),R.color.colorErrorSnackbar,Snackbar.LENGTH_SHORT);
                    }

            }
        });
    }

    private void saveNewUser(UserModel userModel){
        UtilsSharedPreference.save(getApplicationContext(), "session", "true");
        UtilsSharedPreference.save(getApplicationContext(), "firstName", userModel.getFirstName());
        UtilsSharedPreference.save(getApplicationContext(), "lastName", userModel.getLastName());
        UtilsSharedPreference.save(getApplicationContext(), "mail", userModel.getMail());
        UtilsSharedPreference.save(getApplicationContext(), "username", userModel.getUsername());
        UtilsSharedPreference.save(getApplicationContext(), "departament", userModel.getDepartment());
        UtilsSharedPreference.save(getApplicationContext(), "number", userModel.getNumber());
        UtilsSharedPreference.save(getApplicationContext(), "streetAddress", userModel.getStreetAddress());
        UtilsSharedPreference.save(getApplicationContext(), "city", userModel.getCity());
        UtilsSharedPreference.save(getApplicationContext(), "state", userModel.getState());
        UtilsSharedPreference.save(getApplicationContext(), "zipCode", userModel.getZipCode());
    }


    /**
        Implementacion de la Interfaz callback la cual es necesaria para obtener el resultado del llamado al servicio y hacer algo con el dato
        Sea un resultado exitoso o un error de agregar usuario
     */
    void initCallbackRegister(){
        this.callbackRegister = new RecycleResponse() {
            @Override
            public void notifySuccess(String requestType, Object response) {
                Log.d(requestType, response.toString());
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d("CALLBACK", error.toString());
            }
        };
    }


}

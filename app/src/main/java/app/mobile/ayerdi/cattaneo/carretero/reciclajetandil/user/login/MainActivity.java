package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.login;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.VolleyError;

import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.generalView.GeneralViewRecycle;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.system.ColoredSnackbar;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.login.newUser.NewUserActivity;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.R;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.UserModel;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.services.RecycleResponse;

public class MainActivity extends AppCompatActivity {

    RecycleResponse callbackRecycling;
    boolean session;
    boolean doubleBackToExitPressedOnce = false;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide(); //hide the title bar

        session = Boolean.valueOf(UtilsSharedPreference.read(getApplicationContext(), "session", "false"));

        btnLogin = (Button) findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColoredSnackbar.showColoredSnackBar(v, getString(R.string.errorSnackInicioSesion), R.color.colorNotSupportSnackbar, Snackbar.LENGTH_SHORT);
            }
        });

        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        } else if (session){
            final Intent generalView = new Intent(this, GeneralViewRecycle.class);
            final UserModel user = new UserModel();
            user.setFirstName(UtilsSharedPreference.read(getApplicationContext(), "firstName", null));
            user.setLastName(UtilsSharedPreference.read(getApplicationContext(), "lastName", null));
            user.setMail(UtilsSharedPreference.read(getApplicationContext(), "mail", null));
            user.setUsername(UtilsSharedPreference.read(getApplicationContext(), "username", null));
            user.setDepartment(UtilsSharedPreference.read(getApplicationContext(), "departament", null));
            user.setNumber(UtilsSharedPreference.read(getApplicationContext(), "number", null));
            user.setStreetAddress(UtilsSharedPreference.read(getApplicationContext(), "streetAddress", null));
            user.setCity(UtilsSharedPreference.read(getApplicationContext(), "city", null));
            user.setState(UtilsSharedPreference.read(getApplicationContext(), "state", null));
            user.setZipCode(UtilsSharedPreference.read(getApplicationContext(), "zipCode", null));
            generalView.putExtra(GeneralViewRecycle.USER_MODEL, user);
            startActivity(generalView);
        } else {
            Button btnSingUp = (Button) findViewById(R.id.btnNewUser);
            final Intent newUserView = new Intent(this, NewUserActivity.class);
            btnSingUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(newUserView);
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        // Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }


    void initCallBackRecycling(){
        this.callbackRecycling = new RecycleResponse() {
            @Override
            public void notifySuccess(String requestType, Object response) {
                Log.d(requestType, response.toString());
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d("CALLBACK", error.toString());
            }
        };
    }

}

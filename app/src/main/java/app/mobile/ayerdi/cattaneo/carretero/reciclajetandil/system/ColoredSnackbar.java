package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.system;

import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;


public class ColoredSnackbar {

    public static void showColoredSnackBar(View v, String s, int color, int duracion){
        Snackbar snackbar = Snackbar.make(v, s, duracion);
        final View snackBarView = snackbar.getView();
        final ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) snackBarView.getLayoutParams();
        params.setMargins(params.leftMargin + 30,
                params.topMargin,
                params.rightMargin + 30,
                params.bottomMargin + 40);
        snackBarView.setLayoutParams(params);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(snackbar.getContext(), color));
        snackbar.show();
    }
}

package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.generalView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.TextView;

import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.R;

public class RecycleItemView extends Activity {

    private Intent intent;

    public final static String BOTELLA = "BOTELLA";
    public final static String PAPEL = "PAPEL";
    public final static String FECHA = "FECHA";
    public final static String TETRABRICK = "TETRABRICK";
    public final static String LATA = "LATA";
    public final static String VIDRIO = "VIDRIO";

    private TextView botella;
    private TextView papel;
    private TextView fecha;
    private TextView tetrabrick;
    private TextView lata;
    private TextView vidrio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycle_item);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        intent = getIntent();

        botella = findViewById(R.id.textViewBotella);
        papel = findViewById(R.id.textViewPapel);
        fecha = findViewById(R.id.textViewFecha);
        tetrabrick = findViewById(R.id.textViewTetra);
        lata = findViewById(R.id.textViewLata);
        vidrio = findViewById(R.id.textViewVidrio);

        botella.setText(String.valueOf(intent.getIntExtra(BOTELLA, 0)));
        papel.setText(String.valueOf(intent.getIntExtra(PAPEL, 0)));
        fecha.setText(intent.getStringExtra(FECHA));
        tetrabrick.setText(String.valueOf(intent.getIntExtra(TETRABRICK, 0)));
        lata.setText(String.valueOf(intent.getIntExtra(LATA, 0)));
        vidrio.setText(String.valueOf(intent.getIntExtra(VIDRIO, 0)));

    }
}

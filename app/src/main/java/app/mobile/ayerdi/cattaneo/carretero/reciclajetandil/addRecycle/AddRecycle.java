package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.addRecycle;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.R;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.generalView.GeneralViewRecycle;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.services.RecycleResponse;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.services.RecycleService;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.system.ColoredSnackbar;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.login.MainActivity;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.login.UtilsSharedPreference;

public class AddRecycle extends AppCompatActivity {

    private boolean minusActivate = false;
    private static boolean firstTimeClick = true;
    private static final int GET_VALUE_BOTELLA = 1;
    private static final int GET_VALUE_PAPEL = 2;
    private static final int GET_VALUE_VIDRIO = 3;
    private static final int GET_VALUE_LATA = 4;
    private static final int GET_VALUE_TETRA = 5;
    private int counterBotella = 0;
    private int counterPapel = 0;
    private int counterVidrio = 0;
    private int counterLata = 0;
    private int counterTetra = 0;
    private Button btnCounterBotella;
    private Button btnCounterPapel;
    private Button btnCounterVidrio;
    private Button btnCounterLata;
    private Button btnCounterTetra;
    private Button btnBotella;
    private Button btnPapel;
    private Button btnVidrio;
    private Button btnLata;
    private Button btnTetra;
    private Button btnEntregarRecycle;
    private ImageView btnBacktoGeneralView;

    private RecycleResponse callback;
    private String username;

    public static void setfirstTimeClick(boolean b){
        firstTimeClick = b;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recycle);
        getSupportActionBar().hide(); //hide the title bar
        final ImageView btnMinusCounter =  (ImageView) findViewById(R.id.btnMinusCounter);
        final Intent intent = getIntent();
        final Intent setRecycleValue = new Intent(this, SetRecycleValue.class);

        username = UtilsSharedPreference.read(getApplicationContext(), "username", null);

        btnCounterBotella = (Button) findViewById(R.id.btnCounterBotella);
        btnCounterPapel = (Button) findViewById(R.id.btnCounterPapel);
        btnCounterVidrio = (Button) findViewById(R.id.btnCounterVidrio);
        btnCounterLata = (Button) findViewById(R.id.btnCounterLata);
        btnCounterTetra = (Button) findViewById(R.id.btnCounterTetra);
        btnBotella = (Button) findViewById(R.id.btnBotella);
        btnPapel = (Button) findViewById(R.id.btnPapel);
        btnVidrio = (Button) findViewById(R.id.btnVidrio);
        btnLata = (Button) findViewById(R.id.btnLata);
        btnTetra = (Button) findViewById(R.id.btnTetra);
        btnBacktoGeneralView = (ImageView) findViewById(R.id.btnBacktoGeneralView);
        btnEntregarRecycle = (Button) findViewById(R.id.btnEntregarRecycle);

        // BOTON BACK
        btnBacktoGeneralView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // BOTELLA
        btnCounterBotella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!minusActivate){
                    btnCounterBotella.setText(String.valueOf(++counterBotella));
                } else {
                    if (counterBotella != 0)
                        btnCounterBotella.setText(String.valueOf(--counterBotella));
                }
            }
        });
        btnBotella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!minusActivate){
                    btnCounterBotella.setText(String.valueOf(++counterBotella));
                } else {
                    if (counterBotella != 0)
                        btnCounterBotella.setText(String.valueOf(--counterBotella));
                }
            }
        });

        btnCounterBotella.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setRecycleValue.putExtra(SetRecycleValue.SET_VALUE, btnCounterBotella.getText());
                startActivityForResult(setRecycleValue, GET_VALUE_BOTELLA);
                return true;
            }
        });

        //PAPEL
        btnCounterPapel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!minusActivate){
                    btnCounterPapel.setText(String.valueOf(++counterPapel));
                } else {
                    if (counterPapel != 0)
                        btnCounterPapel.setText(String.valueOf(--counterPapel));
                }
            }
        });
        btnPapel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!minusActivate){
                    btnCounterPapel.setText(String.valueOf(++counterPapel));
                } else {
                    if (counterPapel != 0)
                        btnCounterPapel.setText(String.valueOf(--counterPapel));
                }
            }
        });
        btnCounterPapel.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setRecycleValue.putExtra(SetRecycleValue.SET_VALUE, btnCounterPapel.getText());
                startActivityForResult(setRecycleValue, GET_VALUE_PAPEL);
                return true;
            }
        });


        //VIDRIO
        btnCounterVidrio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!minusActivate){
                    btnCounterVidrio.setText(String.valueOf(++counterVidrio));
                } else {
                    if (counterVidrio != 0)
                        btnCounterVidrio.setText(String.valueOf(--counterVidrio));
                }
            }
        });
        btnVidrio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!minusActivate){
                    btnCounterVidrio.setText(String.valueOf(++counterVidrio));
                } else {
                    if (counterVidrio != 0)
                        btnCounterVidrio.setText(String.valueOf(--counterVidrio));
                }
            }
        });
        btnCounterVidrio.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setRecycleValue.putExtra(SetRecycleValue.SET_VALUE, btnCounterVidrio.getText());
                startActivityForResult(setRecycleValue, GET_VALUE_VIDRIO);
                return true;
            }
        });


        //LATA
        btnCounterLata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!minusActivate){
                    btnCounterLata.setText(String.valueOf(++counterLata));
                } else {
                    if (counterLata != 0)
                        btnCounterLata.setText(String.valueOf(--counterLata));
                }
            }
        });
        btnLata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!minusActivate){
                    btnCounterLata.setText(String.valueOf(++counterLata));
                } else {
                    if (counterLata != 0)
                        btnCounterLata.setText(String.valueOf(--counterLata));
                }
            }
        });
        btnCounterLata.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setRecycleValue.putExtra(SetRecycleValue.SET_VALUE, btnCounterLata.getText());
                startActivityForResult(setRecycleValue, GET_VALUE_LATA);
                return true;
            }
        });


        //TETRA
        btnCounterTetra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!minusActivate){
                    btnCounterTetra.setText(String.valueOf(++counterTetra));

                } else {
                    if (counterTetra != 0)
                        btnCounterTetra.setText(String.valueOf(--counterTetra));
                }
            }
        });
        btnTetra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!minusActivate){
                    btnCounterTetra.setText(String.valueOf(++counterTetra));

                } else {
                    if (counterTetra != 0)
                        btnCounterTetra.setText(String.valueOf(--counterTetra));
                }
            }
        });
        btnCounterTetra.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setRecycleValue.putExtra(SetRecycleValue.SET_VALUE, btnCounterTetra.getText());
                startActivityForResult(setRecycleValue, GET_VALUE_TETRA);
                return true;
            }
        });


        // RESET CONTADORES
        btnMinusCounter.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showDialogRestartCounters();
            return true;
            }
        });


        // ACTIVAR CONTADOR NEGATIVO
        btnMinusCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (firstTimeClick){
                    firstTimeClick = false;
                    ColoredSnackbar.showColoredSnackBar(v, getString(R.string.resetFirstClick), R.color.colorMessaggeSnackbar, Snackbar.LENGTH_LONG);
                }
                if (!minusActivate){
                    btnMinusCounter.setImageDrawable(getDrawable(R.drawable.ic_minus_green));
                    minusActivate = true;
                } else {
                    btnMinusCounter.setImageDrawable(getDrawable(R.drawable.ic_minus_white));
                    minusActivate = false;
                }
            }
        });

        btnEntregarRecycle.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if ((counterBotella != 0) || (counterPapel != 0) || (counterLata != 0) || (counterVidrio != 0) || (counterTetra != 0)){
                    showDialogSendRecycle();
                } else {
                    ColoredSnackbar.showColoredSnackBar(v, getString(R.string.errorSnackCero), R.color.colorErrorSnackbar, Snackbar.LENGTH_LONG);
                }
            }
        });
    }

    private void resetCounters(){
        counterBotella = 0;
        counterPapel = 0;
        counterLata = 0;
        counterVidrio = 0;
        counterTetra = 0;
        btnCounterBotella.setText("0");
        btnCounterPapel.setText("0");
        btnCounterLata.setText("0");
        btnCounterVidrio.setText("0");
        btnCounterTetra.setText("0");
    }

    // ENTREGA DE RECICLADO
    private void showDialogSendRecycle(){
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        AlertDialog.Builder alert = new AlertDialog.Builder(AddRecycle.this);
        View mView = getLayoutInflater().inflate(R.layout.activity_confirmation, null);
        alert.setView(mView);
        final AlertDialog alertDialog = alert.create();

        ImageView btnCheck = (ImageView) mView.findViewById(R.id.btnCheck);
        ImageView btnClose = (ImageView) mView.findViewById(R.id.btnClose);
        TextView textConfirmation = (TextView) mView.findViewById(R.id.textConfirmation);

        textConfirmation.setText(R.string.ConfirmRecycle);

        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initCallback();
                Date date = new Date();
                //Log.d("calendar", Calendar.getInstance().getTime().toString());
                //String fecha = date.getYear() + "-" + date.getMonth() + "-" + date.getDay();
                Calendar newDate = Calendar.getInstance();
                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                dateFormatter.format(newDate.getTime());
                String fecha = dateFormatter.format(newDate.getTime());
                RecycleModel recycle = new RecycleModel(fecha, counterBotella, counterPapel, counterVidrio, counterLata, counterTetra);
                RecycleService recycleService = new RecycleService(getApplicationContext(), callback);
                recycleService.addRecycling(recycle, username);

                resetCounters();
                alertDialog.dismiss();

            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        //PERMITE MODIFICAR EL ANCHO POR DEFECTO DEL ALERTDIALOG.BUILDER
            // Get screen width and height in pixels
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            // The absolute width of the available display size in pixels.
            int displayWidth = displayMetrics.widthPixels;
            // Initialize a new window manager layout parameters
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            // Copy the alert dialog window attributes to new layout parameter instance
            layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
            // Set the alert dialog window width and height
            // Set alert dialog width equal to screen width 90%
            // int dialogWindowWidth = (int) (displayWidth * 0.9f);
            // Set alert dialog height equal to screen height 90%
            // int dialogWindowHeight = (int) (displayHeight * 0.9f);
            // Set alert dialog width equal to screen width 70%
            int dialogWindowWidth = (int) (displayWidth * 0.78f);
            // Set the width and height for the layout parameters
            // This will bet the width and height of alert dialog
            layoutParams.width = dialogWindowWidth;
            // Apply the newly created layout parameters to the alert dialog window
            alertDialog.getWindow().setAttributes(layoutParams);
    }

    private void showDialogRestartCounters(){
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        AlertDialog.Builder alert = new AlertDialog.Builder(AddRecycle.this);
        View mView = getLayoutInflater().inflate(R.layout.activity_confirmation, null);
        mView.setBackgroundResource(R.drawable.background_red_card);
        alert.setView(mView);
        final AlertDialog alertDialog = alert.create();
        ImageView btnCheck = (ImageView) mView.findViewById(R.id.btnCheck);
        ImageView btnClose = (ImageView) mView.findViewById(R.id.btnClose);
        TextView textConfirmation = (TextView) mView.findViewById(R.id.textConfirmation);
        textConfirmation.setText(R.string.deleteAllCounter);

        btnCheck.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
                resetCounters();
                alertDialog.dismiss();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
                alertDialog.dismiss();
        }
        });
        alertDialog.show();

        //PERMITE MODIFICAR EL ANCHO POR DEFECTO DEL ALERTDIALOG.BUILDER
            // Get screen width and height in pixels
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            // The absolute width of the available display size in pixels.
            int displayWidth = displayMetrics.widthPixels;
            // Initialize a new window manager layout parameters
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            // Copy the alert dialog window attributes to new layout parameter instance
            layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
            // Set the alert dialog window width and height
            // Set alert dialog width equal to screen width 90%
            // int dialogWindowWidth = (int) (displayWidth * 0.9f);
            // Set alert dialog height equal to screen height 90%
            // int dialogWindowHeight = (int) (displayHeight * 0.9f);
            // Set alert dialog width equal to screen width 70%
            int dialogWindowWidth = (int) (displayWidth * 0.83f);
            // Set the width and height for the layout parameters
            // This will bet the width and height of alert dialog
            layoutParams.width = dialogWindowWidth;
            // Apply the newly created layout parameters to the alert dialog window
            alertDialog.getWindow().setAttributes(layoutParams);
    }



    public void initCallback(){
        this.callback = new RecycleResponse() {
            @Override
            public void notifySuccess(String requestType, Object response) {
                Log.d("SUCCESS", response.toString());
                finish();
                //MOSTRAR MENSAJE RECICLADO AGREGADO CON EXITO
                //REDIRECCIONAR A ALGUN LADO
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d("FAILED", error.toString());
                finish();
                //MOSTRAR MENSAJE ERROR
            }
        };
    }

    // ALMACENAR CONTADORES
    @Override
    protected void onStop() {
        super.onStop();

        UtilsSharedPreference.save(getApplicationContext(), "saveBotella", btnCounterBotella.getText().toString());
        UtilsSharedPreference.save(getApplicationContext(), "savePapel", btnCounterPapel.getText().toString());
        UtilsSharedPreference.save(getApplicationContext(), "saveVidrio", btnCounterVidrio.getText().toString());
        UtilsSharedPreference.save(getApplicationContext(), "saveLata", btnCounterLata.getText().toString());
        UtilsSharedPreference.save(getApplicationContext(), "saveTetra", btnCounterTetra.getText().toString());

    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("saveBotella", btnCounterBotella.getText().toString());
        outState.putString("savePapel", btnCounterPapel.getText().toString());
        outState.putString("saveVidrio", btnCounterVidrio.getText().toString());
        outState.putString("saveLata", btnCounterLata.getText().toString());
        outState.putString("saveTetra", btnCounterTetra.getText().toString());
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    // LEER CONTADORES ALMACENADOS
    @Override
    protected void onStart() {
        super.onStart();
        readValoresContadoresSharedPreferences();
    }
    private void readValoresContadoresSharedPreferences() {

        String savedBotella = UtilsSharedPreference.read(getApplicationContext(), "saveBotella", "0");
        counterBotella = Integer.parseInt(savedBotella);
        btnCounterBotella.setText(savedBotella);

        String savePapel = UtilsSharedPreference.read(getApplicationContext(), "savePapel", "0");
        counterPapel = Integer.parseInt(savePapel);
        btnCounterPapel.setText(savePapel);

        String saveVidrio = UtilsSharedPreference.read(getApplicationContext(), "saveVidrio", "0");
        counterVidrio = Integer.parseInt(saveVidrio);
        btnCounterVidrio.setText(saveVidrio);

        String saveLata = UtilsSharedPreference.read(getApplicationContext(), "saveLata", "0");
        counterLata = Integer.parseInt(saveLata);
        btnCounterLata.setText(saveLata);

        String saveTetra = UtilsSharedPreference.read(getApplicationContext(), "saveTetra", "0");
        counterTetra = Integer.parseInt(saveTetra);
        btnCounterTetra.setText(saveTetra);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null){
            String savedBotella = savedInstanceState.getString("saveBotella");
            counterBotella = Integer.parseInt(savedBotella);
            btnCounterBotella.setText(savedBotella);

            String savePapel = savedInstanceState.getString("savePapel");
            counterPapel = Integer.parseInt(savePapel);
            btnCounterPapel.setText(savePapel);

            String saveVidrio = savedInstanceState.getString("saveVidrio");
            counterVidrio = Integer.parseInt(saveVidrio);
            btnCounterVidrio.setText(saveVidrio);

            String saveLata = savedInstanceState.getString("saveLata");
            counterLata = Integer.parseInt(saveLata);
            btnCounterLata.setText(saveLata);

            String saveTetra = savedInstanceState.getString("saveTetra");
            counterTetra = Integer.parseInt(saveTetra);
            btnCounterTetra.setText(saveTetra);
        }
    }

    // RETORNO DE VALORES
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GET_VALUE_BOTELLA:
                    counterBotella = Integer.valueOf(data.getData().toString()); //Pasarlo primero a integer permite solucionar que el usuario ingrese 0´s al principio del numero
                    btnCounterBotella.setText(String.valueOf(counterBotella));
                    break;
                case GET_VALUE_PAPEL:
                    counterPapel = Integer.valueOf(data.getData().toString());
                    btnCounterPapel.setText(String.valueOf(counterPapel));
                    break;
                case GET_VALUE_VIDRIO:
                    counterVidrio = Integer.valueOf(data.getData().toString());
                    btnCounterVidrio.setText(String.valueOf(counterVidrio));
                    break;
                case GET_VALUE_LATA:
                    counterLata = Integer.valueOf(data.getData().toString());
                    btnCounterLata.setText(String.valueOf(counterLata));
                    break;
                case GET_VALUE_TETRA:
                    counterTetra = Integer.valueOf(data.getData().toString());
                    btnCounterTetra.setText(String.valueOf(counterTetra));
                    break;
            }
        }
    }
}

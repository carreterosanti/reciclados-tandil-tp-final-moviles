package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.services;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.addRecycle.RecycleModel;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.addRecycle.TotalRecycleModel;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.UserModel;


public class RecycleService {

    private String respuesta;
    private final static String BASE_URL = "http://192.168.0.161:8080/api/users/"; // REEMPLAZAR LA DIRECCION IP POR LA CORRESPONDIENTE AL SERVIDOR SIN MODIFICAR http://***.***.***.*** :8080/api/users/
    private Context context;
    private RecycleResponse callback;


    public RecycleService(Context context, RecycleResponse callback){
        this.context = context;
        this.callback = callback;
    }

    public void registerUser(UserModel user){
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = BASE_URL;

        //creacion JSON
        JSONObject ad = new JSONObject();
        try {
            ad.put("department", user.getDepartment());
            ad.put("number", user.getNumber());
            ad.put("streetAddress", user.getStreetAddress());
            ad.put("city", user.getCity());
            ad.put("state", user.getState());
            ad.put("zipCode", user.getZipCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("firstName", user.getFirstName());
            jsonBody.put("lastName", user.getLastName());
            jsonBody.put("mail", user.getMail());
            jsonBody.put("username", user.getUsername());
            jsonBody.put("address", ad);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //fin Creacion JSON

        final String requestBody = jsonBody.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.notifySuccess("POST USER", response);
                        Log.i("--------- SUCCESS:", response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.notifyError("POST USER", error);
                Log.i("------- FAILED:", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        queue.add(stringRequest);
    }

    public void addRecycling(RecycleModel recycle, String username){
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = BASE_URL + username + "/recycling/";

        //creacion JSON
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("bottles", recycle.getCantidadBotella());
            jsonBody.put("tetrabriks", recycle.getCantidadTetra());
            jsonBody.put("glass", recycle.getCantidadVidrio());
            jsonBody.put("paperboard", recycle.getCantidadPapel());
            jsonBody.put("cans", recycle.getCantidadLata());
            jsonBody.put("date", recycle.getFecha());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //fin Creacion JSON

        final String requestBody = jsonBody.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.notifySuccess("ADD RECYCLING SUCCESS", response);
                        Log.i("--------- SUCCESS:", response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.notifyError("ADD RECYLCLING ERROR", error);
                Log.i("------- FAILED:", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        queue.add(stringRequest);
    }

    public void getAllRecyclingUser(String username){

        RequestQueue queue = Volley.newRequestQueue(context);
        String url = BASE_URL + username + "/recycling/";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        respuesta = response;
                        try {
                            ArrayList<RecycleModel> allRecyclingUser = new ArrayList<>();

                            JSONArray respuestaArray = new JSONArray(respuesta);
                            for (int i=0; i<respuestaArray.length(); i++){
                                JSONObject reciclaje = (JSONObject) respuestaArray.get(i);

                                String fecha = (String) reciclaje.get("date");
                                int bottles = (Integer) reciclaje.get("bottles");
                                int tetrabriks = (Integer) reciclaje.get("tetrabriks");
                                int glass = (Integer) reciclaje.get("glass");
                                int paperboard = (Integer) reciclaje.get("paperboard");
                                int cans = (Integer) reciclaje.get("cans");

                                RecycleModel recycle = new RecycleModel(fecha, bottles, paperboard, glass, cans, tetrabriks);
                                allRecyclingUser.add(recycle);
                            }
                            callback.notifySuccess("GET", allRecyclingUser);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                            Log.i("--------- SUCCESS:", response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.notifyError("GET", error);
                Log.i("------- FAILED:", error.toString());
            }
        });
        queue.add(stringRequest);
    }

    public void getTotalRecyclingUser(String username){
        RequestQueue queue = Volley.newRequestQueue(context);

        String url = BASE_URL + username + "/total/";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jO = new JSONObject(response);
                            double tons = (double) jO.get("tons");
                            int bottles = (int) jO.get("bottles");
                            int tetrabriks = (int) jO.get("tetrabriks");
                            int glass = (int) jO.get("glass");
                            int paperboard = (int) jO.get("paperboard");
                            int cans = (int) jO.get("cans");

                            TotalRecycleModel totalRecycle = new TotalRecycleModel(tons, bottles, tetrabriks, glass, paperboard, cans);
                            callback.notifySuccess("GET", totalRecycle);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i("--------- SUCCESS:", response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.notifyError("GET", error);
                Log.i("------- FAILED:", error.toString());
            }
        });
        queue.add(stringRequest);

    }
}

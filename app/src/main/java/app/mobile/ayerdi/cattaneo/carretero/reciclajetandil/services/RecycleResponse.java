package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.services;

import com.android.volley.VolleyError;

public interface RecycleResponse {
    void notifySuccess(String requestType,Object response);
    void notifyError(String requestType,VolleyError error);
}

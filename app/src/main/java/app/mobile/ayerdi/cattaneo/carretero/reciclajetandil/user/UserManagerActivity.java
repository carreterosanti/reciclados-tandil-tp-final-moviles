package app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.addRecycle.AddRecycle;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.generalView.GeneralViewRecycle;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.R;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.login.MainActivity;
import app.mobile.ayerdi.cattaneo.carretero.reciclajetandil.user.login.UtilsSharedPreference;

public class UserManagerActivity extends AppCompatActivity {

    private Button btnQuickLogin;
    private TextView txtNombreCompleto;
    private TextView txtEmail;
    private TextView txtUsername;
    private TextView txtLocation;
    private TextView txtDirection;
    private ImageView btnBacktoGeneralView;
    private Button btnLogOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_manager);
        getSupportActionBar().hide(); //hide the title bar
        final Intent intent = getIntent();

        btnQuickLogin = (Button) findViewById(R.id.btnQuickLoginManager);
        txtNombreCompleto = findViewById(R.id.txtNombreCompletoManager);
        txtEmail = findViewById(R.id.txtEmailManager);
        txtUsername = findViewById(R.id.txtUsernameManager);
        txtLocation = findViewById(R.id.txtLocationManager);
        txtDirection = findViewById(R.id.txtDireccionManager);
        btnBacktoGeneralView = (ImageView) findViewById(R.id.btnBacktoGeneralView);
        btnLogOut = (Button) findViewById(R.id.btnLogOut);

        btnQuickLogin.setText(UtilsSharedPreference.read(getApplicationContext(), "firstName", null).charAt(0)+""+UtilsSharedPreference.read(getApplicationContext(), "lastName", null).charAt(0));
        txtNombreCompleto.setText(UtilsSharedPreference.read(getApplicationContext(), "firstName", null) + " " + UtilsSharedPreference.read(getApplicationContext(), "lastName", null));
        txtEmail.setText(UtilsSharedPreference.read(getApplicationContext(), "mail", null));
        txtUsername.setText(UtilsSharedPreference.read(getApplicationContext(), "username", null));
        txtLocation.setText(UtilsSharedPreference.read(getApplicationContext(), "state", null) + ", " + UtilsSharedPreference.read(getApplicationContext(), "city", null) + ", " + UtilsSharedPreference.read(getApplicationContext(), "zipCode", null));
        txtDirection.setText(UtilsSharedPreference.read(getApplicationContext(), "streetAddress", null));

        btnBacktoGeneralView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogLogOut();
            }
        });
    }

    private void showDialogLogOut(){
        AlertDialog.Builder alert = new AlertDialog.Builder(UserManagerActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.activity_confirmation, null);
        mView.setBackgroundResource(R.drawable.background_red_card);
        alert.setView(mView);
        final AlertDialog alertDialog = alert.create();
        ImageView btnCheck = (ImageView) mView.findViewById(R.id.btnCheck);
        ImageView btnClose = (ImageView) mView.findViewById(R.id.btnClose);
        TextView textConfirmation = (TextView) mView.findViewById(R.id.textConfirmation);
        textConfirmation.setText(R.string.txtLogOut);

        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                GeneralViewRecycle.setfirstTimeUser(true);
                AddRecycle.setfirstTimeClick(true);
                UtilsSharedPreference.clear(getApplicationContext());
                UtilsSharedPreference.save(getApplicationContext(), "session", "false");

                alertDialog.dismiss();
                startActivity(intent);
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

        //PERMITE MODIFICAR EL ANCHO POR DEFECTO DEL ALERTDIALOG.BUILDER
        // Get screen width and height in pixels
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);
        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.65f);
        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }
}
